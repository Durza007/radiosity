#version 150

in vec2 vert_TexCoord;
in vec3 vert_DiffuseColor;
in vec3 vert_DiffuseLight;

out vec4 outColor;

uniform sampler2D tex;
uniform sampler2D multiplier;
uniform float resolution;

void main(void)
{
  if (gl_FrontFacing)
  {
    vec3 textureColor = texture(tex, vert_TexCoord).xyz;
    float multi = texture(multiplier, gl_FragCoord.xy / resolution).r;
    //outColor = vec4(textureColor * vert_DiffuseLight, 1);
    outColor = vec4(vert_DiffuseColor * vert_DiffuseLight * multi, 1);
    //outColor = vec4(vert_DiffuseLight, 1);
    /*float r, g, b;
    // notice the 256's instead of 255
    r = (gl_PrimitiveID % 256) / 255.0f;
    g = ((gl_PrimitiveID / 256) % 256) / 255.0f;
    b = ((gl_PrimitiveID / (256 * 256)) % 256) / 255.0f;
    outColor = vec4(r, g, b, 1);*/
  }
  else
  {
    outColor = vec4(0, 0, 0, 1);
  }
}