-- A solution contains projects, and defines the available configurations
solution "Radiosity"
   configurations { "Debug", "Release" }

   -- A project defines one build target
   project "Radiosity"
      kind "WindowedApp"
      language "C++"
      files { "**.h", "**.cpp", "**.c" }
      links { "SDL2", "SDL2main" }
      defines { "GLEW_STATIC" }

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }

configuration "windows"
   defines "WINDOWS"
   links { "opengl32" }

configuration "linux"
   defines "LINUX"
   buildoptions "-std=c++11"
   links { "GL" }
