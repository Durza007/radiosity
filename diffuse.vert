#version 150

in vec3 in_Position;
in vec2 in_TexCoord;
in vec3 in_DiffuseColor;
in vec3 in_DiffuseLight;

out vec2 vert_TexCoord;
out vec3 vert_DiffuseColor;
out vec3 vert_DiffuseLight;

uniform mat4 projCameraModelMatrix;

void main(void)
{
  vert_TexCoord = in_TexCoord;
  vert_DiffuseColor = in_DiffuseColor;
  vert_DiffuseLight = in_DiffuseLight;
	gl_Position = projCameraModelMatrix * vec4(in_Position, 1.0);
}