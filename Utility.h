#pragma once

#include <fstream>
#include <string>
#include "VectorUtils3.h"

// Simple file containing som utility functions
namespace Utility
{
	std::string readTextFile(const char *filename);
	mat4 ortho(float left, float right, float bottom, float top, float near, float far);

	inline
	int max(int a, int b)
	{
		return a > b ? a : b;
	}

	inline
	vec3 getVertex(GLfloat *vertexArray, int index)
	{
		return vec3{
			vertexArray[index * 3 + 0],
			vertexArray[index * 3 + 1],
			vertexArray[index * 3 + 2]
		};
	}

	inline
	void printVector(vec3 v)
	{
		printf("x: %f, y: %f, z: %f\n", v.x, v.y, v.z);
	}
}
