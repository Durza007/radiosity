#include "Font.h"

#include "json.h"
#include "LoadTGA.h"
#include "Utility.h"

using namespace json;

Font::Font(const char* file)
{
	// Load JSON file
	std::string fileString{ file };
	std::string fontString = Utility::readTextFile(file);

	Value font = Deserialize(fontString);

	{ // Load font texture
		// Figure out path to texture
		std::string imageFile = font["file"];
		auto index = fileString.find_last_of("/\\");
		std::string file;
		if (index == std::string::npos)
		{
			file = imageFile;
		}
		else
		{
			file = fileString.substr(0, index) + std::string{ "/" } +imageFile;
		}

		// Load texture
		TextureData data;
		LoadTGATexture((char*)file.c_str(), &data);

		// Get image properties
		texture = data.texID;
		imageWidth = data.w;
		imageHeight = data.h;

		// Set texture filtering settings
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}

	// Load info about all the characters
	charCount = (char)(font["count"].ToInt() - 1);
	chars = new Char[charCount];

	Array charObjects = font["chars"];

	int index = 0;
	maxHeight = 0;
	for (Object charObject : charObjects)
	{
		chars[index].id = charObject["id"].ToInt();
		chars[index].x = charObject["x"].ToInt() - 2;
		chars[index].y = charObject["y"].ToInt();
		chars[index].width = charObject["width"].ToInt();
		chars[index].height = charObject["height"].ToInt();
		chars[index].xOffset = charObject["xoffset"].ToInt();
		chars[index].yOffset = charObject["yoffset"].ToInt();
		chars[index].xAdvance = charObject["xadvance"].ToInt();

		maxHeight = Utility::max(maxHeight, chars[index].height);

		if (chars[index].id == -1)
		{
			unknownChar = chars[index];
		}
		else
		{
			index += 1;
		}
	}

	// Create buffers
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vb);
	glGenBuffers(1, &tb);
}

Font::~Font()
{
	delete[] chars;
}

const Char* Font::getChar(char c) const
{
	for (int index = 0; index < charCount; index++)
	{
		if (chars[index].id == c)
		{
			return &chars[index];
		}
	}

	return &unknownChar;
}

int Font::getLineHeight() const
{
	return this->maxHeight;
}

int Font::getWidth(std::string text) const
{
	return getWidth(text.c_str());
}

int Font::getWidth(const char* text) const
{
	int width = 0;
	int index = 0;
	while (true)
	{
		if (text[index] == '\0') break;

		const Char* c = getChar(text[index]);
		width += c->xAdvance;

		index += 1;
	}

	return width;
}

void Font::build(std::string text, int x, int y, Alignment alignment)
{
	this->build(text.c_str(), x, y, alignment);
}
 
void Font::build(const char* text, int x, int y, Alignment alignment)
{
	// Relics. Kept here because they might be of some use someday
	int screenWidth = 1;
	int screenHeight = 1;

	int xOffset = 0;
	if (alignment == CENTER || alignment == RIGHT)
	{
		// In order to get "center-/right- alignment" correct
		// we need to know the length of the string
		xOffset = -getWidth(text);

		if (alignment == CENTER)
		{
			xOffset /= 2;
		}
	}

	// Go through the string and build vertex buffers
	int index = 0;
	int currX = x;
	while (true)
	{
		// Null terminated strings. End on null
		if (text[index] == '\0') break;

		// Get next char
		const Char* c = getChar(text[index]);

		// Build quad as shown
		/*
		1 x-----x 2
		  | \   |
		  |   \ |
		0 x-----x 3
		*/
		// Triangle 1
		// 0
		vertices.push_back((currX + c->xOffset + xOffset) / (float)screenWidth);
		vertices.push_back((y + c->yOffset) / (float)screenHeight);

		// 1
		vertices.push_back((currX + c->xOffset + xOffset) / (float)screenWidth);
		vertices.push_back((y + c->yOffset + c->height) / (float)screenHeight);

		// 3
		vertices.push_back((currX + c->xOffset + c->width + xOffset) / (float)screenWidth);
		vertices.push_back((y + c->yOffset) / (float)screenHeight);

		// Triangle 2
		// 3
		vertices.push_back((currX + c->xOffset + c->width + xOffset) / (float)screenWidth);
		vertices.push_back((y + c->yOffset) / (float)screenHeight);

		// 1
		vertices.push_back((currX + c->xOffset + xOffset) / (float)screenWidth);
		vertices.push_back((y + c->yOffset + c->height) / (float)screenHeight);

		// 2
		vertices.push_back((currX + c->xOffset + c->width + xOffset) / (float)screenWidth);
		vertices.push_back((y + c->yOffset + c->height) / (float)screenHeight);

		// Create texcoords
		GLfloat zeroX = c->x / (float)imageWidth;
		GLfloat zeroY = c->y / (float)imageHeight;
		GLfloat oneX = c->x / (float)imageWidth;
		GLfloat oneY = (c->y + c->height) / (float)imageHeight;
		GLfloat twoX = (c->x + c->width) / (float)imageWidth;
		GLfloat twoY = (c->y + c->height) / (float)imageHeight;
		GLfloat threeX = (c->x + c->width) / (float)imageWidth;
		GLfloat threeY = c->y / (float)imageHeight;

		// 0
		texCoords.push_back(zeroX);
		texCoords.push_back(zeroY);

		// 1
		texCoords.push_back(oneX);
		texCoords.push_back(oneY);

		// 3
		texCoords.push_back(threeX);
		texCoords.push_back(threeY);

		// 3
		texCoords.push_back(threeX);
		texCoords.push_back(threeY);

		// 1
		texCoords.push_back(oneX);
		texCoords.push_back(oneY);

		// 2
		texCoords.push_back(twoX);
		texCoords.push_back(twoY);

		// Advance to the next char
		index += 1;
		currX += c->xAdvance;
	}
}

void Font::draw(GLuint program, const char* vertexVariableName, const char* texCoordVariableName) const
{
	GLint loc;

	glBindVertexArray(vao);
	// Vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vb);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STREAM_DRAW);

	loc = glGetAttribLocation(program, vertexVariableName);
	if (loc >= 0)
	{
		glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(loc);
	}

	// Texture Coordinate buffer
	glBindBuffer(GL_ARRAY_BUFFER, tb);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), texCoords.data(), GL_STREAM_DRAW);

	loc = glGetAttribLocation(program, texCoordVariableName);
	if (loc >= 0)
	{
		glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(loc);
	}

	glBindTexture(GL_TEXTURE_2D, texture);
	glDrawArrays(GL_TRIANGLES, 0, vertices.size() / 2);
}

void Font::clear()
{
	vertices.clear();
	texCoords.clear();
}