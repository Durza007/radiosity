#include "Utility.h"

std::string Utility::readTextFile(const char *filename)
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	if (in)
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
		return(contents);
	}
	throw(errno);
}

// Crate orthogonal projection matrix
mat4 Utility::ortho(float left, float right, float bottom, float top, float near, float far)
{
	float temp1, temp2, temp3;
	mat4 matrix;

	temp1 = right - left;
	temp2 = top - bottom;
	temp3 = far - near;

	matrix.m[0] = 2.0 / temp1; // 2 / (right-left)
	matrix.m[1] = 0.0;
	matrix.m[2] = 0.0;
	matrix.m[3] = 0.0;
	matrix.m[4] = 0.0;
	matrix.m[5] = 2.0 / temp2; // 2 / (top - bottom)
	matrix.m[6] = 0.0;
	matrix.m[7] = 0.0;
	matrix.m[8] = 0.0;
	matrix.m[9] = 0.0;
	matrix.m[10] = -2.0 / temp3; // -2 / f-n
	matrix.m[11] = 0.0;
	matrix.m[12] = -(right + left) / temp1;	// -(right + left) / (right - left)
	matrix.m[13] = -(top + bottom) / temp2;	// -(top + bottom) / (top - bottom)
	matrix.m[14] = (-far * near) / temp3;	// -(far + near) / (far - near)
	matrix.m[15] = 1.0;

	matrix = Transpose(matrix);

	return matrix;
}