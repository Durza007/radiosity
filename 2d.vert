#version 150

in vec3 in_Position;
in vec2 in_TexCoord;

out vec2 vert_TexCoord;

uniform mat4 projMatrix;

void main(void)
{
	vert_TexCoord = in_TexCoord;
	gl_Position = projMatrix * vec4(in_Position, 1.0);
}