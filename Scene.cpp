#include "Scene.h"

#include <fstream>
#include <string>
#include <cerrno>
#include <math.h>
#include "json.h"
#include "Utility.h"

using namespace json;
using namespace std;

vec3 jsonArrayToVec3(const Array& array)
{
	return vec3{ array[0].ToFloat(), array[1].ToFloat(), array[2].ToFloat() };
}

vec3 createPlane(vec3 A, vec3 B, vec3 C, vec3 D, float patchSize, bool subdivide, vector<vec3>& vertices, vector<unsigned int>& indices)
{
	// Get vectors from which the plane will be built
	vec3 i = D - A;
	vec3 j = B - A;
	vec3 normal = Normalize(CrossProduct(i, j));

	// Figure out how much subdivisions we need to match the requested patchSize
	int subdivisionX;
	int subdivisionY;
	if (subdivide)
	{
		subdivisionX = roundf(Norm(i) / patchSize);
		subdivisionY = roundf(Norm(j) / patchSize);

		if (subdivisionX < 1) subdivisionX = 1;
		if (subdivisionY < 1) subdivisionY = 1;
	}
	else
	{
		subdivisionX = 1;
		subdivisionY = 1;
	}

	// Create grid of vertices
	for (int y = 0; y < subdivisionY + 1; y++)
	{
		for (int x = 0; x < subdivisionX + 1; x++)
		{
			vec3 p = A + i * x / (float)subdivisionX + j * y / (float)subdivisionY;
			vertices.push_back(p);
		}
	}

	// Create triangles
	for (int y = 0; y < subdivisionY; y++)
	{
		for (int x = 0; x < subdivisionX; x++)
		{
			/*
			1/B x-----x 2/C
			    | \   |
			    |   \ |
			0/A x-----x 3/D
			*/

			// Counter clock-wise rotation
			indices.push_back(y * (subdivisionX + 1) + x); // 0
			indices.push_back(y * (subdivisionX + 1) + x + 1); // 3
			indices.push_back((y + 1) * (subdivisionX + 1) + x); // 1
			indices.push_back((y + 1) * (subdivisionX + 1) + x); // 1
			indices.push_back(y * (subdivisionX + 1) + x + 1); // 3
			indices.push_back((y + 1) * (subdivisionX + 1) + x + 1); // 2
		}
	}

	return normal;
}

Scene::Scene(const char* file, float patchSize)
{
	fileName = std::string{ file };
	this->patchSize = patchSize;

	std::string sceneString = Utility::readTextFile(file);

	Value scene = Deserialize(sceneString);

	Value scaleValue = scene["Scale"];
	float scale = 1.0f;
	if (scaleValue.GetType() == DoubleVal)
	{
		scale = scaleValue.ToFloat();
	}

	Value cameraPos = scene["Camera"];
	if (cameraPos.GetType() == ArrayVal)
	{
		defaultCameraPos = jsonArrayToVec3(cameraPos) * scale;
	}

	Array objects = scene["Geometry"];
	
	for (Object shape : objects)
	{
		int indexOffset = vertices.size();
		printf("[INFO]: Parsing plane %s\n", shape["name"].ToString().c_str());

		Object properties = shape["Properties"];
		vec3 diffuseColor = jsonArrayToVec3(properties["DiffuseColor"]);
		vec3 emission = jsonArrayToVec3(properties["Emission"]);
		vec3 reflectance = jsonArrayToVec3(properties["Reflectance"]);

		bool subdivide = true;

		// Check if it's a plane
		if (shape["A"].GetType() != NULLVal)
		{
			// Corners of the plane
			vec3 A = jsonArrayToVec3(shape["A"]) * scale;
			vec3 B = jsonArrayToVec3(shape["B"]) * scale;
			vec3 C = jsonArrayToVec3(shape["C"]) * scale;
			vec3 D = jsonArrayToVec3(shape["D"]) * scale;

			vector<vec3> newVertices;
			vector<unsigned int> newIndices;

			// If the plane does not reflect light then it does not need to be a part of the
			// hemicube rendering. Optimize by not subdividing this plane
			if (reflectance.x == 0.0f && reflectance.y == 0.0f && reflectance.z == 0.0f)
			{
				subdivide = false;
			}

			vec3 normal = createPlane(A, B, C, D, patchSize, subdivide, newVertices, newIndices);

			for (vec3 vertex : newVertices)
			{
				vertices.push_back(vertex);
				normals.push_back(normal);
				diffuseColors.push_back(diffuseColor);
				reflectances.push_back(reflectance);
				emissions.push_back(emission);
				excidents.push_back(vec3{ 0, 0, 0 });
			}

			for (unsigned int index : newIndices)
			{
				indices.push_back(indexOffset + index);
			}
		}
		else
		{
			// Create sphere by making a cube where the vertices are normalized and
			// multiplied by the radius.
			vec3 coords[][4] = {
				{ // +X
					vec3{ 1, -1, 1 },
					vec3{ 1, 1, 1 },
					vec3{ 1, 1, -1 },
					vec3{ 1, -1, -1 },
				},
				{ // -X
					vec3{ -1, 1, 1 },
					vec3{ -1, -1, 1 },
					vec3{ -1, -1, -1 },
					vec3{ -1, 1, -1 },
				},
				{ // +Y
					vec3{ 1, 1, 1 },
					vec3{ -1, 1, 1 },
					vec3{ -1, 1, -1 },
					vec3{ 1, 1, -1 },
				},
				{ // -Y
					vec3{ 1, -1, -1 },
					vec3{ -1, -1, -1 },
					vec3{ -1, -1, 1 },
					vec3{ 1, -1, 1 },
				},
				{ // +Z
					vec3{ -1, -1, 1 },
					vec3{ -1, 1, 1 },
					vec3{ 1, 1, 1 },
					vec3{ 1, -1, 1 },
				},
				{ // -Z
					vec3{ -1, 1, -1 },
					vec3{ -1, -1, -1 },
					vec3{ 1, -1, -1 },
					vec3{ 1, 1, -1 },
				},
			};

			// Get sphere properties
			vec3 pos = jsonArrayToVec3(shape["Pos"]) * scale;
			float radius = shape["Radius"];

			vector<vec3> newVertices;
			vector<unsigned int> newIndices;

			// Create sphere
			for (auto& planeCoords : coords)
			{
				vec3 normal = createPlane(
					planeCoords[0],
					planeCoords[1],
					planeCoords[2],
					planeCoords[3],
					patchSize / radius, subdivide, newVertices, newIndices);

				for (vec3 vertex : newVertices)
				{
					vertices.push_back(Normalize(vertex) * radius + pos);
					normals.push_back(Normalize(vertex));
					diffuseColors.push_back(diffuseColor);
					reflectances.push_back(reflectance);
					emissions.push_back(emission);
					excidents.push_back(emission);
				}

				for (unsigned int index : newIndices)
				{
					indices.push_back(indexOffset + index);
				}

				indexOffset = vertices.size();
				newVertices.clear();
				newIndices.clear();
			}
		}
	}

	/*
	 * Upload data to GPU
	*/
	// Create buffers
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vb);
	glGenBuffers(1, &ib);
	//glGenBuffers(1, &this->nb);
	glGenBuffers(1, &db);
	glGenBuffers(1, &lb);

	glBindVertexArray(vao);

	// Vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vb);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

	// Normal buffer
	//glBindBuffer(GL_ARRAY_BUFFER, nb);
	//glBufferData(GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(GLfloat), normals.data(), GL_STATIC_DRAW);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

	// Diffuse color buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->db);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(GLfloat), diffuseColors.data(), GL_STATIC_DRAW);

	// Light buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->lb);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(GLfloat), excidents.data(), GL_DYNAMIC_DRAW);
}

Scene::~Scene()
{
	glDeleteBuffers(1, &vb);
	glDeleteBuffers(1, &ib);
	glDeleteBuffers(1, &db);
	glDeleteBuffers(1, &lb);
	glDeleteVertexArrays(1, &vao);
}

vec3 Scene::getDefaultCameraPos() const
{
	return this->defaultCameraPos;
}

void Scene::resetExcidents()
{
	for (int index = 0; index < vertices.size(); index++)
	{
		excidents[index] = emissions[index];
	}
}

void Scene::uploadExcidents()
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, lb);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(GLfloat), excidents.data(), GL_DYNAMIC_DRAW);
}

void Scene::draw(GLuint program, const char* vertexVariableName, const char* diffuseColorVariableName, const char* diffuseLightVariableName)
{
	GLint loc;
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vb);
	loc = glGetAttribLocation(program, vertexVariableName);
	if (loc >= 0)
	{
		glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(loc);
	}

	glBindBuffer(GL_ARRAY_BUFFER, db);
	loc = glGetAttribLocation(program, diffuseColorVariableName);
	if (loc >= 0)
	{
		glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(loc);
	}

	glBindBuffer(GL_ARRAY_BUFFER, lb);
	loc = glGetAttribLocation(program, diffuseLightVariableName);
	if (loc >= 0)
	{
		glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(loc);
	}

	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0L);
}

std::string getSaveName(std::string fileName)
{
	auto index = fileName.find_last_of(".");
	return fileName.substr(0, index) + std::string{ ".save" };
}

void Scene::saveLight() const
{
	std::string saveName = getSaveName(fileName);

	std::ofstream out(saveName, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
	out.write((char*)&patchSize, sizeof(float));
	for (auto light : excidents)
	{
		out.write((char*)&light, sizeof(vec3));
	}

	out.close();

	printf("Wrote to: %s\n", saveName.c_str());
}

Scene* Scene::loadLight() const
{
	std::string saveName = getSaveName(fileName);

	std::ifstream in{ saveName, std::ios_base::binary };

	in.read((char*)&patchSize, sizeof(float));

	Scene* newScene = new Scene(fileName.c_str(), patchSize);

	for (int index = 0; index < newScene->excidents.size(); index++)
	{
		in.read((char*)&newScene->excidents[index], sizeof(vec3));
	}
	in.close();

	printf("Read from: %s\n", saveName.c_str());

	return newScene;
}
