#include <string>
#include <vector>
#include "glew.h"

struct Char
{
	int x, y;
	int width, height;
	int xOffset, yOffset;
	int xAdvance;
	char id;
};

class Font
{
public:
	enum Alignment {LEFT, CENTER, RIGHT};

	// Init class with name of JSON file that contains Font information
	Font(const char* file);
	~Font();

	// Getters for text properties
	int getLineHeight() const;
	int getWidth(std::string text) const;
	int getWidth(const char* text) const;

	// Build a line of text that will be added to the draw buffer
	void build(std::string text, int x, int y, Alignment alignment = LEFT);
	void build(const char* text, int x, int y, Alignment alignment = LEFT);

	// Draw all text in draw buffer
	void draw(GLuint program, const char* vertexVariableName, const char* texCoordVariableName) const;
	
	// Clear draw buffer
	void clear();

private:
	// Default char for unknown characters
	Char unknownChar;
	int imageWidth;
	int imageHeight;

	Char* chars;
	int maxHeight;
	int charCount;

	std::vector<GLfloat> vertices;
	std::vector<GLfloat> texCoords;

	GLuint texture;
	GLuint vao;
	GLuint vb, tb;

	const Char* getChar(char c) const;
};