#include <string>
#include <vector>

#include "glew.h"
#include "VectorUtils3.h"

class Scene
{
public:
	Scene(const char* file, float patchSize = 1.0f);
	~Scene();

	vec3 getDefaultCameraPos() const;

	// Reset all the light
	void resetExcidents();

	// Upload light to the GPU
	void uploadExcidents();

	// Draw scene
	void draw(GLuint program, const char* vertexVariableName, const char* diffuseColorVariableName, const char* diffuseLightVariableName);

	// Save light to binary file "fileName.save"
	void saveLight() const;

	// Load light from binary file
	// Return a new Scene containing the loaded light
	Scene* loadLight() const;

	std::vector<vec3> vertices;
	std::vector<vec3> normals;
	std::vector<unsigned int> indices;
	std::vector<vec3> diffuseColors;
	std::vector<vec3> reflectances;
	std::vector<vec3> emissions;
	std::vector<vec3> excidents;

private:
	std::string fileName;
	vec3 defaultCameraPos{ 0, 0, 0 };
	float patchSize;

	// VAO
	GLuint vao;
	// VBOs
	GLuint vb, ib, nb, db, lb;
};
