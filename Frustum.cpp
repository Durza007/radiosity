#include "Frustum.h"

using namespace Frustum;

inline
void normalizePlane(vec4& plane)
{
	float length = sqrt(plane.x * plane.x + plane.y * plane.y + plane.z * plane.z);

	plane /= length;
}

Parameters Frustum::getFrustum(const mat4& projectionView)
{
	Parameters f;
	f.left = row(projectionView, 3) + row(projectionView, 0);
	f.right = row(projectionView, 3) - row(projectionView, 0);
	f.bottom = row(projectionView, 3) + row(projectionView, 1);
	f.top = row(projectionView, 3) - row(projectionView, 1);
	f.far = row(projectionView, 3) - row(projectionView, 2);

	normalizePlane(f.left);
	normalizePlane(f.right);
	normalizePlane(f.bottom);
	normalizePlane(f.top);
	normalizePlane(f.far);

	return f;
}

int Frustum::checkSphereFrustum(const Parameters& f, vec3 pos, float radius)
{
	// Far plane
	if (-radius > (pos.x * f.far.x + pos.y * f.far.y + pos.z * f.far.z + f.far.w))
		return false;

	// Left plane
	if (-radius > (pos.x * f.left.x + pos.y * f.left.y + pos.z * f.left.z + f.left.w))
		return false;

	// Right plane
	if (-radius > (pos.x * f.right.x + pos.y * f.right.y + pos.z * f.right.z + f.right.w))
		return false;

	// Top plane
	if (-radius > (pos.x * f.top.x + pos.y * f.top.y + pos.z * f.top.z + f.top.w))
		return false;

	// Bottom plane
	if (-radius > (pos.x * f.bottom.x + pos.y * f.bottom.y + pos.z * f.bottom.z + f.bottom.w))
		return false;

	return true;
}