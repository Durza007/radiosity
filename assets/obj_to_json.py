vertices = []
currentName = "Unknown"

def printCorner(name, vertex):
    x = vertices[int(vertex) - 1][1]
    y = vertices[int(vertex) - 1][2]
    z = vertices[int(vertex) - 1][3]
    print("  \"" + name + "\":[" + str(x) + ", " + str(y) + ", " + str(z) + "],")

print("{")
print("\"Camera\":[0.0, 3.0, 0.0],")
print("\"Scale\":1.0,")
print("\"Geometry\":[")

for line in open("new_room.obj",'r'):
    if line.find("v ") > -1:
        info = line.split()
        vertices.append(info)
    elif line.find("o ") > -1:
        currentName = line[2:][:-1]
    elif line.find("f ") > -1:
        clean = line.replace("//", "")
        face = clean.split()
        print("{\n  \"name\":\"" + currentName + "\",")
        printCorner("A", face[1])
        printCorner("B", face[4])
        printCorner("C", face[3])
        printCorner("D", face[2])
        
        print("  \"Properties\":")
        print("    {")
        print("      \"DiffuseColor\":[1.0, 1.0, 1.0],")
        print("      \"Emission\":[0.0, 0.0, 0.0],")
        print("      \"Reflectance\":[0.7, 0.7, 0.7]")
        print("    }")
        print("},")
        
print("]\n}")
#print(currentPlane)
