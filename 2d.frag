#version 150

in vec2 vert_TexCoord;

out vec4 outColor;

uniform sampler2D tex;

void main(void)
{
  outColor = texture(tex, vert_TexCoord);
}