#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include <float.h>
//unsigned int fp_control_state = _controlfp(_EM_INEXACT, _MCW_EM);
#include <math.h>

#include <SDL2/SDL.h>
#include "glew.h"

#include "GL_utilities.h"
#include "VectorUtils3.h"
#include "loadobj.h"
#include "LoadTGA.h"
#include "Scene.h"
#include "Font.h"
#include "Frustum.h"
#include "Utility.h"

int screenWidth = 1024;
int screenHeight = 1024;
mat4 projectionMatrix;

// Camera variables
const float CAMERA_ACCELERATION = 0.01f;
const float CAMERA_MAX_SPEED = 0.2f;
float cameraRx = 0.0f;
float cameraRy = 0.0f;
vec3 cameraPos{ 0, 0, 0 };
vec3 cameraVel{ 0, 0, 0 };
vec3 cameraDir{ 0, 0, 0 };
vec3 upVector{ 0, 1, 0 };
float exposure = 1.0f;

// Calculate projection matrices used for hemicube rendering
const float size = 0.1f;
const float halfSize = size / 2.0f;
const mat4 projForward = frustum(-halfSize, halfSize, -halfSize, halfSize, halfSize, 200.0);
const mat4 projLeft = frustum(0, halfSize, -halfSize, halfSize, halfSize, 200.0);
const mat4 projRight = frustum(-halfSize, 0, -halfSize, halfSize, halfSize, 200.0);
const mat4 projBottom = frustum(-halfSize, halfSize, 0, halfSize, halfSize, 200.0);
const mat4 projTop = frustum(-halfSize, halfSize, -halfSize, 0, halfSize, 200.0);

const int SCENE_COUNT = 2;
const char* SCENES[] = {
	"assets/cornell_box.json",
	"assets/simple_room.json"
};

float patchSize = 1.0f;
Scene* scene = nullptr;
Font* font = nullptr;
// Vertex array object
Model* arrow;
// Reference to shader program
GLuint program2D, program2DFont, programDiffuse, programDiffuseExposure;

// Framebuffer globals
const int resolution = 256;
GLfloat* pixels;
GLfloat* multiplier;
GLuint frameBuffer;
GLuint frameBufferTexture;
GLuint frameBufferDepthBuffer;
GLuint multiplierTexture;
GLuint whiteTexture;

GLuint normalsVAO;
GLuint normalsVBO;
int count = -1;

// Forward decleration
void clearRadiosity();

void calculateProjectionMatrix()
{
	const float h = 0.1f;
	// Calculate width based on window aspect ratio
	const float w = h * (screenWidth / (float)screenHeight);
	projectionMatrix = frustum(-w, w, -h, h, 0.2, 200.0);
}

// Inits frameBuffer and form factor multiplier texture
bool initFrameBuffer()
{
	// TODO: Move this out to seperate functions
	pixels = new GLfloat[resolution * resolution * 3];
	multiplier = new GLfloat[resolution * resolution];

	// Start by clearing the image
	for (int index = 0; index < resolution * resolution; index++)
	{
		multiplier[index] = 0.0f;
	}

	// Hemicube looking along x-axis. y-axis to the left side
	// and z-axis up
	vec3 forward{ 1, 0, 0 };
	vec3 side{ 0, 1, 0 };

	// Do the middle
	int quarter = resolution / 4;
	int center = resolution / 2;
	for (int y = quarter; y < quarter * 3; y++)
	{
		float zCoord = (y - center) / (float)quarter;

		for (int x = quarter; x < quarter * 3; x++)
		{
			float yCoord = (x - center) / (float)quarter;

			vec3 pixel{ 1, yCoord, zCoord };
			float lambert = DotProduct(Normalize(pixel), forward);
			// Value for compensating for the hemicube distortion
			float distortion = lambert;

			multiplier[y * resolution + x] = lambert * distortion;
		}
	}

	// The four sides
	center = 0;
	for (int y = quarter; y < quarter * 3; y++)
	{
		float zCoord = (y - resolution / 2) / (float)quarter;

		for (int x = 0; x < quarter; x++)
		{
			float xCoord = (x - center) / (float)quarter;

			vec3 pixel{ xCoord, 1, zCoord };
			float lambert = DotProduct(Normalize(pixel), forward);
			float distortion = DotProduct(Normalize(pixel), side);

			multiplier[y * resolution + x] = lambert * distortion;

			// Because of symmetry we can just use the same value for the rest of the sides
			multiplier[y * resolution + resolution - 1 - x] = multiplier[y * resolution + x];
			multiplier[x * resolution + y] = multiplier[y * resolution + x];
			multiplier[(resolution - 1 - x) * resolution + y] = multiplier[y * resolution + x];
		}
	}

	// Normalize the image
	float total = 0.0f;
	for (int index = 0; index < resolution * resolution; index++)
	{
		total += multiplier[index];
	}
	for (int index = 0; index < resolution * resolution; index++)
	{
		multiplier[index] /= total;
	}

	// Upload multiplier data
	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &multiplierTexture);
	glBindTexture(GL_TEXTURE_2D, multiplierTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, resolution, resolution, 0, GL_RED, GL_FLOAT, multiplier);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// Just to be sure
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);

	// Create a white texture which can replace the multiplier texture
	// when a user is looking at the "hemicube view" or else everything
	// will look dark
	glGenTextures(1, &whiteTexture);
	glBindTexture(GL_TEXTURE_2D, whiteTexture);

	const float color = 1.0;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, 1, 1, 0, GL_RED, GL_FLOAT, &color);

	// reset the active texture
	glActiveTexture(GL_TEXTURE0);

	printError("Upload Mulitplier");

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	// The texture we're going to render to
	glGenTextures(1, &frameBufferTexture);

	// Bind newly created texture
	glBindTexture(GL_TEXTURE_2D, frameBufferTexture);

	// Create empty image.
	// Use 16 bit floats so that values are not clamped to [0, 1]
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, resolution, resolution, 0, GL_RGB, GL_FLOAT, 0);

	// Do not want linear filtering since it messes with our data
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// Just to be sure
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);

	// The depth buffer
	glGenRenderbuffers(1, &frameBufferDepthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, frameBufferDepthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, resolution, resolution);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, frameBufferDepthBuffer);

	// Set "frameBufferTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, frameBufferTexture, 0);

	// Set the list of draw buffers.
	GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	// Make sure everything is ok
	bool isOk = glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;

	// Change back to default framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	printError("Init framebuffer");
	return isOk;
}

void init()
{
	// GL inits
	//glClearColor(0.2, 0.2, 0.5, 0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	// Need blending for text rendering
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	printError("GL inits");

	// Init default projection matrix
	calculateProjectionMatrix();

	// Load and compile shaders
	program2D = loadShaders("2d.vert", "2d.frag");
	program2DFont = loadShaders("2d.vert", "2dFont.frag");
	programDiffuse = loadShaders("diffuse.vert", "diffuse.frag");
	programDiffuseExposure = loadShaders("diffuse.vert", "diffuseExposure.frag");

	// Set some uniforms for the shaders
	glUseProgram(program2D);
	glUniform1i(glGetUniformLocation(program2D, "tex"), 0); // Texture unit 0

	glUseProgram(program2DFont);
	glUniform1i(glGetUniformLocation(program2DFont, "tex"), 0); // Texture unit 0

	glUseProgram(programDiffuse);
	glUniform1i(glGetUniformLocation(programDiffuse, "tex"), 0); // Texture unit 0
	glUniform1i(glGetUniformLocation(programDiffuse, "multiplier"), 1); // Texture unit 0
	glUniform1f(glGetUniformLocation(programDiffuse, "resolution"), (float)resolution);

	glUseProgram(programDiffuseExposure);
	glUniform1i(glGetUniformLocation(programDiffuseExposure, "tex"), 0); // Texture unit 0
	glUniform1f(glGetUniformLocation(programDiffuseExposure, "exposure"), 1.0f);

	printError("init shader");

	font = new Font("assets/font2.json");

	//arrow = LoadModelPlus("assets/arrow.obj");
	//room = LoadModelPlus("assets/room.obj");

	printError("init end");
}

// Loads and initializes a scene
void loadScene(const char* scenePath, float patchSize)
{
	if (scene != nullptr)
	{
		delete scene;
	}

	scene = new Scene(scenePath, patchSize);
	cameraPos = scene->getDefaultCameraPos();

	printf("Num vertices: %d\n", scene->vertices.size());
	printError("init scene");

	// Create normal lines of room model. Used for debugging.
	std::vector<vec3> lines;
	for (int index = 0; index < scene->vertices.size(); index++)
	{
		vec3 vertex = scene->vertices[index];
		vec3 normal = scene->normals[index];

		vec3 normalPoint = VectorAdd(vertex, normal);

		lines.push_back(vertex);
		lines.push_back(normalPoint);
	}

	// Check if we already created vao- and vbo-id
	if (count == -1)
	{
		glGenVertexArrays(1, &normalsVAO);
		glBindVertexArray(normalsVAO);
		glGenBuffers(1, &normalsVBO);
	}
	else
	{
		glBindVertexArray(normalsVAO);
	}

	count = lines.size();

	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, normalsVBO);
	glBufferData(GL_ARRAY_BUFFER, 3 * lines.size() * sizeof(GLfloat), &lines[0], GL_STATIC_DRAW);
	glVertexAttribPointer(glGetAttribLocation(programDiffuse, "in_Position"), 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(glGetAttribLocation(programDiffuse, "in_Position"));

	// Reset light data so we have a clean slate
	clearRadiosity();

	printError("LoadScene");
}

// TODO: Remove/refactor globals
bool forward, back, left, right = false;
bool next, previous = false;
bool freeCam = true;
bool wireframe = false;
bool normals = false;
bool showHelp = false;
bool changePatchSize = false;
int radiosityIterations = 0;

// Update logic
void update()
{
	// Vertex index used for when user wants to view
	// from hemicube perspective
	static int index = 0;

	// Update camera
	cameraDir = SetVector(0, 0, 1);

	mat4 rx, ry;
	rx = Rx(cameraRx);
	ry = Ry(cameraRy);
	cameraDir = MultVec3(Mult(ry, rx), cameraDir);

	// Calculate camera velocity
	vec3 forwardVector = cameraDir * CAMERA_ACCELERATION;
	vec3 sideVector = Normalize(CrossProduct(upVector, forwardVector)) * CAMERA_ACCELERATION;;
	if (forward)
	{
		cameraVel += forwardVector;
	}
	if (back)
	{
		cameraVel -= forwardVector;
	}
	if (left)
	{
		cameraVel += sideVector;
	}
	if (right)
	{
		cameraVel -= sideVector;
	}

	// Make sure camera does not move to fast
	float speed = Norm(cameraVel);
	if (speed > CAMERA_MAX_SPEED)
	{
		cameraVel *= CAMERA_MAX_SPEED / speed;
	}

	// Update camera pos
	cameraPos += cameraVel;
	cameraVel *= 0.9f;

	// Update selected hemicube
	if (next)
	{
		index += 1;
	}
	if (previous)
	{
		index -= 1;
	}

	// Checking array bounds
	if (index < 0) index = scene->vertices.size() - 1;
	if (index >= scene->vertices.size()) index = 0;

	// Position camera at vertex if user has chosen that
	if (!freeCam)
	{
		cameraPos = scene->vertices[index];
		cameraDir = scene->normals[index];
	}

	// Draw text on screen
	int lineHeight = font->getLineHeight();
	font->build("h - Show help", 0, 0);
	
	// Chech if user is changing patch size
	if (changePatchSize)
	{
		// Draw current patch size
		std::string size = std::to_string(patchSize);
		font->build(std::string("Patch Size: ") + size, screenWidth / 2, screenHeight / 2, Font::CENTER);
	}
	else
	{
		// Otherwise we draw normal text
		std::string iterations = std::to_string(radiosityIterations);

		// Check if user wants the help text
		if (showHelp)
		{
			// Lots of help-text
			font->build("1 - Free cam", 0, lineHeight);
			font->build("2 - Hemicube cam", 0, lineHeight * 2);
			font->build("q - Previous hemicube", 20, lineHeight * 3);
			font->build("e - Next hemicube", 20, lineHeight * 4);
			font->build("3 - Toggle wireframe", 0, lineHeight * 5);
			font->build("4 - Toggle normals", 0, lineHeight * 6);

			font->build("i - Iterate radiosity", 0, lineHeight * 8);
			font->build("c - Clear radiosity", 0, lineHeight * 9);

			font->build("p - Hold and move mouse", 0, lineHeight * 11);
			font->build("to change patchSize", font->getWidth("p - "), lineHeight * 12);

			font->build("F9 - Save light", 0, lineHeight * 15);
			font->build("F10 - Load light", 0, lineHeight * 16);

			font->build("w,a,s,d - Move free cam", 0, screenHeight - lineHeight * 2);
			font->build("Left mouse - Aim free cam", 0, screenHeight - lineHeight);

			font->build(std::string("Iterations: ") + iterations, screenWidth, 0, Font::RIGHT);
		}
		else
		{
			font->build(iterations, screenWidth, 0, Font::RIGHT);
		}
	}
}

// Renders the actual scene
void renderWorld(const mat4& projectionMatrix, const vec3& cameraPos, const vec3& cameraDir, const vec3& cameraUp, GLuint program)
{
	mat4 total, modelView, camMatrix;

	printError("pre display");

	// Create camera matrix
	vec3 lookAtPoint = VectorAdd(cameraPos, cameraDir);
	mat4 camera = lookAtv(cameraPos, lookAtPoint, cameraUp);

	// Create view-frustum for frustum-culling
	//mat4 projectionView = Mult(projectionMatrix, camera);
	//Frustum::Parameters f = Frustum::getFrustum(projectionView);

	glUseProgram(program);
	modelView = IdentityMatrix();
	total = projectionMatrix * camera * modelView;
	glUniformMatrix4fv(glGetUniformLocation(program, "projCameraModelMatrix"), 1, GL_TRUE, total.m);

	// Maybe render normals (only for debugging)
	if (normals)
	{
		glBindVertexArray(normalsVAO);
		glDrawArrays(GL_LINES, 0, count);
	}

	/*
	 * RENDER SCENE
	*/
	scene->draw(program, "in_Position", "in_DiffuseColor", "in_DiffuseLight");
}

// Render hemicube to the frameBuffer texture
void renderHemicube(const vec3& pos, const vec3& dir)
{
	// Get the current viewport settings
	// TODO: Avoid get because it is slow
	int viewPort[4];
	glGetIntegerv(GL_VIEWPORT, viewPort);

	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	int quarter = resolution / 4;

	vec3 up = upVector;
	vec3 leftDir = CrossProduct(up, dir);
	// Handle if up vector and dir is pointing along the same axis
	if (Norm(leftDir) == 0.0f) {
		up = vec3{ 1, 0, 0 };
		leftDir = CrossProduct(up, dir);
	}

	vec3 topDir = CrossProduct(dir, leftDir);

	// Render from all 5 perspectives
	// Forward
	glViewport(quarter, quarter, quarter * 2, quarter * 2);
	renderWorld(projForward, pos, dir, up, programDiffuse);

	// Left
	glViewport(0, quarter, quarter, quarter * 2);
	renderWorld(projLeft, pos, leftDir, topDir, programDiffuse);

	// Right
	glViewport(quarter * 3, quarter, quarter, quarter * 2);
	renderWorld(projRight, pos, -leftDir, topDir, programDiffuse);

	// Top
	glViewport(quarter, quarter * 3, quarter * 2, quarter);
	renderWorld(projTop, pos, topDir, -dir, programDiffuse);

	// Bottom
	glViewport(quarter, 0, quarter * 2, quarter);
	renderWorld(projBottom, pos, -topDir, dir, programDiffuse);

	// Set the viewport to what it was
	glViewport(viewPort[0], viewPort[1], viewPort[2], viewPort[3]);
}

// Do one radiosity iteration
void iterateRadiosity()
{
	// Select the multiplierTexture
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, multiplierTexture);
	glActiveTexture(GL_TEXTURE0);

	// Iterate over all vertices in the scene
	int quarter = resolution / 4;
	for (int index = 0; index < scene->vertices.size(); index++)
	{
		vec3 reflectance = scene->reflectances[index];
		// Skip vertices that do not reflect anything
		if (reflectance.x == 0.0f && reflectance.y == 0.0f && reflectance.z == 0.0f) continue;

		vec3 pos = scene->vertices[index];
		vec3 normal = scene->normals[index];

		// Render the world from the point of view of the vertex
		renderHemicube(pos, normal);

		// Fetch the image
		glBindTexture(GL_TEXTURE_2D, frameBufferTexture);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, pixels);

		// Calculate the sum of all pixels
		float r = 0;
		float g = 0;
		float b = 0;
		for (int y = 0; y < quarter; y++)
		{
			for (int x = quarter; x < quarter * 3; x++)
			{
				const int pixel = y * resolution + x;
				r += pixels[pixel * 3 + 0];
				g += pixels[pixel * 3 + 1];
				b += pixels[pixel * 3 + 2];
			}
		}
		for (int y = quarter; y < quarter * 3; y++)
		{
			for (int x = 0; x < resolution; x++)
			{
				const int pixel = y * resolution + x;
				r += pixels[pixel * 3 + 0];
				g += pixels[pixel * 3 + 1];
				b += pixels[pixel * 3 + 2];
			}
		}
		for (int y = quarter * 3; y < resolution; y++)
		{
			for (int x = quarter; x < quarter * 3; x++)
			{
				const int pixel = y * resolution + x;
				r += pixels[pixel * 3 + 0];
				g += pixels[pixel * 3 + 1];
				b += pixels[pixel * 3 + 2];
			}
		}

		/*for (int pixel = 0; pixel < resolution * resolution; pixel++)
		{
			r += pixels[pixel * 3 + 0];
			g += pixels[pixel * 3 + 1];
			b += pixels[pixel * 3 + 2];
		}*/
		
		// Calculate the resulting outgoing light of the vertex
		vec3 emission = scene->emissions[index];
		scene->excidents[index].x = r * reflectance.x + emission.x;
		scene->excidents[index].y = g * reflectance.y + emission.y;
		scene->excidents[index].z = b * reflectance.z + emission.z;
	}

	// Upload new data to the gpu
	scene->uploadExcidents();

	// Keep track of how many iterations we have done
	radiosityIterations += 1;
}

// Reset radiosity light
void clearRadiosity()
{
	scene->resetExcidents();
	scene->uploadExcidents();

	radiosityIterations = 0;
}

// Draw a texture to the screen
void drawTexture(GLuint texture, int x, int y, int width, int height)
{
	// Check if the drawTexture extension is available
	if (GLEW_NV_draw_texture)
	{
		glDrawTextureNV(texture, 0, x, y, x + width, y + height, 0, 0, 0, 1, 1);
	}
	else
	{
		/* 
		 * Ugly fallback for the drawtexture extension 
		 */
		glUseProgram(program2D);
		glBindTexture(GL_TEXTURE_2D, texture);

		mat4 ortho = Utility::ortho(0, screenWidth, 0, screenHeight, -1, 1);
		glUniformMatrix4fv(glGetUniformLocation(program2D, "projMatrix"), 1, GL_TRUE, ortho.m);

		float x1 = x;
		float x2 = x + width;
		float y1 = y;
		float y2 = y + height;
		GLfloat vertices[] = {
			x1, y1, 0,
			x2, y1, 0,
			x1, y2, 0,
			x2, y2, 0
		};

		GLfloat texCoords[] = {
			0, 0,
			1, 0, 
			0, 1,
			1, 1
		};

		// init things
		// (I did not feel like adding more global state so buffers are generated on
		// on each call. This function is called at most once per frame so it is not a problem)
		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		GLuint vertexBufferObjID;
		GLuint texCoordBufferObjID;
		glGenBuffers(1, &vertexBufferObjID);

		// VBO for vertex data
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjID);
		glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), vertices, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(glGetAttribLocation(program2D, "in_Position"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(glGetAttribLocation(program2D, "in_Position"));

		// Allocate texture Buffer Objects
		glGenBuffers(1, &texCoordBufferObjID);

		// VBO for texture data
		glBindBuffer(GL_ARRAY_BUFFER, texCoordBufferObjID);
		glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), texCoords, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(glGetAttribLocation(program2D, "in_TexCoord"), 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(glGetAttribLocation(program2D, "in_TexCoord"));

		// draw texture quad
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// cleanup
		glDeleteBuffers(1, &vertexBufferObjID);
		glDeleteBuffers(1, &texCoordBufferObjID);
		glDeleteVertexArrays(1, &vao);
	}
}

// Renders the normal view that the user sees
void display()
{
	//GLfloat t = (GLfloat)glutGet(GLUT_ELAPSED_TIME) / 1000.0;

	// Check if user wants wireframe mode
	if (wireframe)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	// Render hemicube for the selected vertex (For debugging and demonstration)
	if (!freeCam)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, whiteTexture);
		glActiveTexture(GL_TEXTURE0);
		renderHemicube(cameraPos, cameraDir);
	}

	// Switch to default frambuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	printError("display mid");

	// Either render the world or render a hemicube texture
	if (freeCam)
	{
		glUseProgram(programDiffuseExposure);
		glUniform1f(glGetUniformLocation(programDiffuseExposure, "exposure"), exposure);
		renderWorld(projectionMatrix, cameraPos, cameraDir, upVector, programDiffuseExposure);
	}
	else
	{
		/*glBindTexture(GL_TEXTURE_2D, multiplierTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, resolution, resolution, 0, GL_RED, GL_FLOAT, multiplier);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_RED);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_RED);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ONE);*/

		// Scale texture to fit but keep aspect ratio
		int size;
		if (screenWidth < screenHeight)
			size = screenWidth;
		else
			size = screenHeight;

		drawTexture(frameBufferTexture, (screenWidth - size) / 2, (screenHeight - size) / 2, size, size);
	}

	// Render text
	glDisable(GL_DEPTH_TEST);
	glUseProgram(program2DFont);

	mat4 ortho = Utility::ortho(0, screenWidth, screenHeight, 0, -1, 1);
	glUniformMatrix4fv(glGetUniformLocation(program2DFont, "projMatrix"), 1, GL_TRUE, ortho.m);

	vec4 color{ 1, 1, 1, 1 };
	glUniform4fv(glGetUniformLocation(program2DFont, "color"), 1, (GLfloat*)&color);

	font->draw(program2DFont, "in_Position", "in_TexCoord");
	font->clear();

	glEnable(GL_DEPTH_TEST);

	printError("display end");
}

/* A simple function that prints a message, the error code returned by SDL,
* and quits the application */
void sdldie(const char *msg)
{
	printf("%s: %s\n", msg, SDL_GetError());
	SDL_Quit();
	exit(1);
}


void checkSDLError(int line = -1)
{
#ifndef NDEBUG
	const char *error = SDL_GetError();
	if (*error != '\0')
	{
		printf("SDL Error: %s\n", error);
		if (line != -1)
			printf(" + line: %i\n", line);
		SDL_ClearError();
	}
#endif
}


/* Our program's entry point */
int main(int argc, char *argv[])
{
	// Figure out which scene to load
	char* sceneToLoad = "assets/cornell_box.json";
	if (argc == 2)
	{
		sceneToLoad = argv[1];
	}

	// Window handle
	SDL_Window *mainWindow;
	// OpenGL context handle
	SDL_GLContext mainContext;

	// Init SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		sdldie("Unable to initialize SDL");

	// Request opengl 3.2 core context
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// Turn on double buffering with a 24bit Z buffer.
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	// Create the window
	mainWindow = SDL_CreateWindow(
		"Radiosity",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		screenWidth,
		screenHeight,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	// Check if window was created sucessfully
	if (!mainWindow)
		sdldie("Unable to create window");

	checkSDLError(__LINE__);

	// Create an opengl context and attach it to the window
	mainContext = SDL_GL_CreateContext(mainWindow);
	checkSDLError(__LINE__);

	// TODO: Error handling
	glewExperimental = true;
	glewInit();
	printError("GLEW init. According to the internet this error will always be here =/");

	// Start to init the interesting stuff
	init();
	loadScene(sceneToLoad, patchSize);

	// Init framebuffer used for hemicube rendering
	if (!initFrameBuffer())
	{
		sdldie("Failed to create frambuffer =(");
	}

	dumpInfo();

	/* This makes our buffer swap syncronized with the monitor's vertical refresh */
	SDL_GL_SetSwapInterval(1);

	// Main loop flag
	bool quit = false;

	// Event handler
	SDL_Event e;

	bool readMouse = false;
	bool moveCamera = false;

	// SDL_SetRelativeMouseMode does not work correctly
	// on Ubuntu for me =(
	bool useMouseGrabFallback = false;

	/*float start = (float)SDL_GetTicks();
	for (int i = 0; i < 16; i++)
	{
		iterateRadiosity();
		printf("=");
	}
	clearRadiosity();
	printf("\nTime: %f\n", (((float)SDL_GetTicks()) - start) / 1000.0);*/

	// While application is running
	while (!quit)
	{
		next = previous = false;

		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			switch (e.type)
			{
			case SDL_QUIT:
				quit = true;
				break;

			case SDL_WINDOWEVENT:
				if (e.window.event == SDL_WINDOWEVENT_RESIZED)
				{
					screenWidth = e.window.data1;
					screenHeight = e.window.data2;
					glViewport(0, 0, screenWidth, screenHeight);
					calculateProjectionMatrix();
				}
				break;

			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				if (e.button.button == SDL_BUTTON_LEFT || e.button.button == SDL_BUTTON_RIGHT)
				{
					readMouse = e.type == SDL_MOUSEBUTTONDOWN;
					moveCamera = e.button.button == SDL_BUTTON_LEFT;
					if (!useMouseGrabFallback && SDL_SetRelativeMouseMode((SDL_bool)readMouse) != 0)
					{
						SDL_SetRelativeMouseMode(SDL_FALSE);
						useMouseGrabFallback = true;
					}

					if (useMouseGrabFallback)
					{
						SDL_WarpMouseInWindow(mainWindow, screenWidth / 2, screenHeight / 2);
					}
				}
				break;

			case SDL_MOUSEMOTION:
				if (readMouse)
				{
					int xrel = e.motion.xrel;
					int yrel = e.motion.yrel;
					if (useMouseGrabFallback)
					{
						xrel = e.motion.x - screenWidth / 2;
						yrel = e.motion.y - screenHeight / 2;

						if (xrel != 0 || yrel != 0)
						{
							SDL_WarpMouseInWindow(mainWindow, screenWidth / 2, screenHeight / 2);
						}
					}

					if (moveCamera)
					{
						cameraRy += xrel / 500.0f;
						cameraRx += yrel / 500.0f;
					}
					else
					{
						exposure -= yrel / 1000.0f;
					}

				}
				if (changePatchSize)
				{
					patchSize -= e.motion.yrel / 1500.0f;
				}
				break;

			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_w)
				{
					forward = true;
				}
				if (e.key.keysym.sym == SDLK_s)
				{
					back = true;
				}
				if (e.key.keysym.sym == SDLK_a)
				{
					left = true;
				}
				if (e.key.keysym.sym == SDLK_d)
				{
					right = true;
				}
				if (e.key.keysym.sym == SDLK_1)
				{
					freeCam = true;
				}
				if (e.key.keysym.sym == SDLK_2)
				{
					freeCam = false;
				}
				if (e.key.keysym.sym == SDLK_3 && e.key.repeat == 0)
				{
					wireframe = !wireframe;
				}
				if (e.key.keysym.sym == SDLK_4 && e.key.repeat == 0)
				{
					normals = !normals;
				}
				if (e.key.keysym.sym == SDLK_q && e.key.repeat == 0)
				{
					previous = true;
				}
				if (e.key.keysym.sym == SDLK_e && e.key.repeat == 0)
				{
					next = true;
				}
				if (e.key.keysym.sym == SDLK_i && e.key.repeat == 0)
				{
					iterateRadiosity();
				}
				if (e.key.keysym.sym == SDLK_c && e.key.repeat == 0)
				{
					clearRadiosity();;
				}
				if (e.key.keysym.sym == SDLK_h && e.key.repeat == 0)
				{
					showHelp = !showHelp;
				}
				if (e.key.keysym.sym == SDLK_F9 && e.key.repeat == 0)
				{
					scene->saveLight();
				}
				if (e.key.keysym.sym == SDLK_F10 && e.key.repeat == 0)
				{
					Scene* newScene = scene->loadLight();
					delete scene;
					scene = newScene;
					scene->uploadExcidents();
				}
				if (e.key.keysym.sym == SDLK_p)
				{
					changePatchSize = true;
				}
				break;
			case SDL_KEYUP:
				switch (e.key.keysym.sym)
				{
				case SDLK_w:
					forward = false;
					break;

				case SDLK_s:
					back = false;
					break;

				case SDLK_a:
					left = false;
					break;

				case SDLK_d:
					right = false;
					break;

				case SDLK_p:
					changePatchSize = false;
					loadScene(sceneToLoad, patchSize);
					break;

				default:
					break;
				}
				break;
			}
		}

		// Update logic
		update();

		// Render view
		display();

		//Update screen
		SDL_GL_SwapWindow(mainWindow);
	}

	/* Delete our opengl context, destroy our window, and shutdown SDL */
	SDL_GL_DeleteContext(mainContext);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();

	return 0;
}
