Course project in TSBK07 by Patrik Minder (patmi094)

Code written by me can be found in the following files:
  main.cpp
  Scene.h
  Scene.cpp
  Font.h
  Font.cpp
  Utility.h
  Utility.cpp
  2d.frag
  2d.vert
  2dFont.frag
  diffuse.frag
  diffuse.vert
  diffuseExposure.frag

I also wrote these two (Not actually used):
  Frustum.h
  Frustum.cpp

All other files come from libraries or the lab-series in course.
Some minor changes were made in VectorUtils.

Project files are generated with Premake4 (https://premake.github.io/)
Project is defined in the file called premake4.lua

Project requires SDL2 to be installed
