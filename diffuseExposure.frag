#version 150

in vec2 vert_TexCoord;
in vec3 vert_DiffuseColor;
in vec3 vert_DiffuseLight;

out vec4 outColor;

uniform sampler2D tex;
uniform float exposure;

void main(void)
{
  if (gl_FrontFacing)
  {
    vec3 textureColor = texture(tex, vert_TexCoord).xyz;
    vec3 exposedLight = (vec3(1.0f) - exp(-vert_DiffuseLight * exposure));
    outColor = vec4(vert_DiffuseColor * exposedLight, 1);
  }
  else
  {
    outColor = vec4(0, 0, 0, 1);
  }
}