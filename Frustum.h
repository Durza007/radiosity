#pragma once
#include "VectorUtils3.h"

namespace Frustum
{
	struct Parameters
	{
		vec4 far{ 0, 0, 0, 0 };
		vec4 left{ 0, 0, 0, 0 };
		vec4 right{ 0, 0, 0, 0 };
		vec4 top{ 0, 0, 0, 0 };
		vec4 bottom{ 0, 0, 0, 0 };

		Parameters()
		{
		}
	};

	Parameters getFrustum(const mat4& projectionView);
	int checkSphereFrustum(const Parameters& f, vec3 pos, float radius);
}