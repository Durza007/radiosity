#version 150

in vec2 vert_TexCoord;

out vec4 outColor;

uniform sampler2D tex;
uniform vec4 color;

void main(void)
{
  vec4 temp = color;
  temp.a *= texture(tex, vert_TexCoord).r;
  outColor = temp;
}